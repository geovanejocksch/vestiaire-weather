package jocksch.geovane.vestiairewheater

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import jocksch.geovane.forecast_feature.forecast.view.FORECAST_LIST_FRAGMENT_TAG
import jocksch.geovane.forecast_feature.forecast.view.ForecastListFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        val fragment = ForecastListFragment()
        // #Key notes
        // By default I would use Navigation from Jetpack to handle the fragment stack.
        // But as we only have one transition in the entire app, I don't think the effort is worth.
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment, FORECAST_LIST_FRAGMENT_TAG)
            .commit()
    }
}
