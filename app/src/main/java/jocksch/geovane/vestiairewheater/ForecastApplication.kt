package jocksch.geovane.vestiairewheater

import android.app.Application
import jocksch.geovane.forecast_data.di.forecastDataModule
import jocksch.geovane.forecast_feature.forecast.di.forecastFeatureModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class ForecastApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        setupKoin()
    }

    private fun setupKoin() {
        startKoin {
            androidContext(this@ForecastApplication)
            modules(
                forecastDataModule,
                forecastFeatureModule
            )
        }
    }
}
