package jocksch.geovane.forecast_feature.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.verify
import jocksch.geovane.forecast_data.model.Forecast
import jocksch.geovane.forecast_data.repository.ForecastRepository
import jocksch.geovane.forecast_feature.CoroutineTestRule
import jocksch.geovane.forecast_feature.forecast.viewmodel.ForecastListViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class ForecastListViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    var coroutineTestRule = CoroutineTestRule()

    @MockK
    private lateinit var repository: ForecastRepository

    private lateinit var coroutineContext: CoroutineContext

    private lateinit var forecastListViewModelList: ForecastListViewModel

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        MockKAnnotations.init(this)
        coroutineContext = coroutineTestRule.testDispatcher
        forecastListViewModelList = ForecastListViewModel(repository, coroutineContext)
    }

    @Test
    fun `should load forecast and populate list`() {

        val expectedResponse = listOf<Forecast>()

        every {
            runBlocking(coroutineContext) {
                repository.getMostRecentForecasts()
            }
        } returns expectedResponse

        val loading = forecastListViewModelList.isLoading.testObserver
        val forecasts = forecastListViewModelList.forecasts.testObserver

        forecastListViewModelList.loadForecasts()

        verify {
            loading.onChanged(true)
            forecasts.onChanged(expectedResponse)
            loading.onChanged(false)
        }
    }

    @Test
    fun `should get error when load forecast and propagate it`() {

        val expectedError = Exception("Unable to make api call")

        every {
            runBlocking(coroutineContext) {
                repository.getMostRecentForecasts()
            }
        } throws expectedError

        val loading = forecastListViewModelList.isLoading.testObserver
        val error = forecastListViewModelList.error.testObserver

        forecastListViewModelList.loadForecasts()

        verify {
            loading.onChanged(true)
            error.onChanged(expectedError.message)
            loading.onChanged(false)
        }
    }
}

val <T> LiveData<T>.testObserver get() = mockk<Observer<T>>(relaxed = true).also(this::observeForever)
