package jocksch.geovane.forecast_feature.forecast.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import jocksch.geovane.forecast_data.model.Forecast
import jocksch.geovane.forecast_data.repository.ForecastRepository
import kotlinx.coroutines.launch
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class ForecastListViewModel(
    private val repository: ForecastRepository,
    private val coroutineContext: CoroutineContext
) : ViewModel() {

    private val _forecasts = MutableLiveData<List<Forecast>>(listOf())
    val forecasts: LiveData<List<Forecast>> get() = _forecasts

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> get() = _isLoading

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> get() = _error

    private val _selectedForecast = MutableLiveData<Forecast>()
    val selectedForecast: LiveData<Forecast> get() = _selectedForecast

    fun loadForecasts() {
        viewModelScope.launch(coroutineContext) {
            try {
                _isLoading.postValue(true)
                _forecasts.postValue(repository.getMostRecentForecasts())
            } catch (e: Exception) {
                // Here handle the error. Maybe use a LiveEvent?
                _error.postValue(e.message)
            }finally {
                _isLoading.postValue(false)
            }
        }
    }

}
