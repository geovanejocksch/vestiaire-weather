package jocksch.geovane.forecast_feature.forecast.di

import jocksch.geovane.forecast_data.model.Forecast
import jocksch.geovane.forecast_feature.forecast.viewmodel.ForecastDetailViewModel
import jocksch.geovane.forecast_feature.forecast.viewmodel.ForecastListViewModel
import kotlinx.coroutines.Dispatchers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val forecastFeatureModule = module {
    viewModel { ForecastListViewModel(get(), Dispatchers.IO) }
    viewModel { (forecast: Forecast) -> ForecastDetailViewModel(forecast) }
}
