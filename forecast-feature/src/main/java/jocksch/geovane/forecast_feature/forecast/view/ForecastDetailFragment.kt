package jocksch.geovane.forecast_feature.forecast.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import jocksch.geovane.forecast_data.model.Forecast
import jocksch.geovane.forecast_feature.databinding.FragmentForecastDetailBinding
import jocksch.geovane.forecast_feature.forecast.viewmodel.ForecastDetailViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ForecastDetailFragment : Fragment() {

    private val viewModel: ForecastDetailViewModel by viewModel {
        parametersOf(
            arguments?.getSerializable(
                FORECAST_EXTRA
            ) as Forecast
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentForecastDetailBinding.inflate(inflater).also {
        it.viewModel = viewModel
        it.lifecycleOwner = viewLifecycleOwner
    }.root

    companion object {

        const val FORECAST_EXTRA = "FORECAST-EXTRA"

        fun newInstance(forecast: Forecast): ForecastDetailFragment {
            return ForecastDetailFragment().apply {
                val bundle = Bundle()
                bundle.putSerializable(FORECAST_EXTRA, forecast)
                arguments = bundle
            }
        }
    }
}
