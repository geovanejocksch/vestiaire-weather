package jocksch.geovane.forecast_feature.forecast.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import jocksch.geovane.forecast_data.model.Forecast
import jocksch.geovane.forecast_feature.R
import jocksch.geovane.forecast_feature.databinding.ItemForecastBinding
import kotlinx.android.synthetic.main.item_forecast.view.*

class ForecastAdapter(
    private val items: List<Forecast>,
    private val listener: (Forecast) -> Unit
) : RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ForecastViewHolder(
            ItemForecastBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) =
        holder.onBind(items[position], listener)

    class ForecastViewHolder(private val binding: ItemForecastBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(forecast: Forecast, listener: (Forecast) -> Unit) {
            binding.forecast = forecast
            binding.executePendingBindings()
            binding.root.setOnClickListener { listener.invoke(forecast) }
        }
    }
}
