package jocksch.geovane.forecast_feature.forecast.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.BaseTransientBottomBar
import jocksch.geovane.forecast_data.model.Forecast
import jocksch.geovane.forecast_feature.R
import jocksch.geovane.forecast_feature.databinding.FragmentForecastListBinding
import jocksch.geovane.forecast_feature.forecast.viewmodel.ForecastListViewModel
import jocksch.geovane.utils.replaceWithAnimation
import org.koin.androidx.viewmodel.ext.android.viewModel

const val FORECAST_LIST_FRAGMENT_TAG = "ForecastListFragment"

class ForecastListFragment : Fragment() {

    private val viewModel: ForecastListViewModel by viewModel()
    private var binding: FragmentForecastListBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentForecastListBinding.inflate(
        inflater
    ).also {
        it.viewModel = viewModel
        it.lifecycleOwner = viewLifecycleOwner
        binding = it
    }.root

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadForecasts()
    }

    override fun onResume() {
        super.onResume()
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.error.observe(this, Observer {
            binding?.let {
                Snackbar.make(it.root, R.string.generic_error, BaseTransientBottomBar.LENGTH_LONG).show()
            }
        })

        viewModel.forecasts.observe(this, Observer {
            binding?.forecastRecycler?.adapter = ForecastAdapter(
                it, ::navigateToDetail
            )
        })
    }

    private fun navigateToDetail(forecast: Forecast) {
        val fragment = ForecastDetailFragment.newInstance(forecast)
        replaceWithAnimation(fragment, R.id.container)
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

}
