package jocksch.geovane.forecast_feature.forecast.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import jocksch.geovane.forecast_data.model.Forecast
import jocksch.geovane.forecast_feature.R
import jocksch.geovane.utils.kelvinToCelsius

class ForecastDetailViewModel(private val forecast: Forecast) : ViewModel() {

    val forecastContent: LiveData<Forecast> = MutableLiveData<Forecast>(forecast)

    val message: Int
        get() {
            return when {
                temperature > 25 -> R.string.forecast_detail_hot_message
                temperature < 10 -> R.string.forecast_detail_cold_message
                else -> R.string.forecast_detail_default_message
            }
        }

    val temperature: Int
        get() = forecast.temp.day.kelvinToCelsius()
}
