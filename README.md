Vestiaire Weather Test
-----------------------------

#### Modules
Just a small explanation about the modules, I choose to split every feature into data/feature. The data contains the domain functions, whiile the feature contains the logic for the view.

- **app**: Main module, where the application starts and that commands every other modules.
- **forecast-data**: This module contains all the data related to forecasts. Contains repository, datasource and services for every forecast API call.
- **forecast-feature**: This module contains the feature itself, with the views (Fragment, Adapter, xml and ViewModel). It get the necessary data from forecast-data module
- **utils**: Contains utils, such as functions for conversion, temperature handling and binding adapters. In this case is being used only for forecast-data and forecast-feature, but if more features would be add, they would probably use functions from this module.

#### Architecture
The app is implemented following the MVVM pattern, with databinding. That means all the logic for a feature should be on ViewModel, and the values should be observed from the xml using Databinding.
In the data side, the app follows a DataSource/Repository pattern.

#### Libraries 
- **ktlint**: Static Analysis tool focused in code style
- **detekt**: Static Analysis tool to ensure code quality
- **retrofit & okHttp**: Libs used for API calls
- **Koin**: Lib used for dependency Injection
- **Coroutines**: Used to handle threads, such as background threads for API calls
- **Glide**: Used to load image urls into ImageViews
- **MockK**: Test library for mocking.

#### CI
This repository contaiins a basic CI configuration using Gitlab CI, with the following states:
- **detekt**: Run detekt task, a static analysis tool to ensure quality.
- **ktlint**: Run ktlint task, another static analysis but focused in code style.
- **Test**: Run the unit tests presents in the app.
- **Build**: Try to build the app, to ensure that no broken build was commited.
