package jocksch.geovane.utils

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("loadImage")
fun ImageView.loadImage(url: String) {
    Glide.with(this)
        .load(url)
        .into(this)
}

@BindingAdapter("visible")
fun View.visible(isVisible: Boolean) {
    visibility = if(isVisible){
        View.VISIBLE
    } else {
        View.GONE
    }
}
