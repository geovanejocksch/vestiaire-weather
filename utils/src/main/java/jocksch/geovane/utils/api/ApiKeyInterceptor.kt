package jocksch.geovane.utils.api

import okhttp3.Interceptor
import okhttp3.Response

const val API_KEY_HEADER = "appid"
const val API_KEY_VALUE = "648a3aac37935e5b45e09727df728ac2"

class ApiKeyInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val originalRequest = chain.request()
        val originalHttp = originalRequest.url
        val newUrl = originalHttp.newBuilder().addQueryParameter(
            API_KEY_HEADER,
            API_KEY_VALUE
        ).build()

        val newRequest = chain.request().newBuilder()
            .url(newUrl)
            .build()
        return chain.proceed(newRequest)
    }

}
