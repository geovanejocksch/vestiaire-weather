package jocksch.geovane.utils.api

import jocksch.geovane.utils.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val TIMEOUT = 6000L
private const val BASE_URL = "https://api.openweathermap.org/data/2.5/"

fun <T> getApi(
    serviceClass: Class<T>
): T {

    // Configure okHttp client
    val client = OkHttpClient.Builder()
    client.readTimeout(TIMEOUT, TimeUnit.SECONDS)
    client.connectTimeout(TIMEOUT, TimeUnit.SECONDS)

    // Log only when debug
    if (BuildConfig.DEBUG) {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        client.addInterceptor(logging)
    }

    client.addInterceptor(ApiKeyInterceptor())

    // Build retrofit
    val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client.build())
        .addConverterFactory(GsonConverterFactory.create())

    return retrofit.build().create(serviceClass)
}
