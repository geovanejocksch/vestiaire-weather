package jocksch.geovane.utils

import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.concurrent.TimeUnit

const val KELVIN_TO_CELSIUS = 273.15

fun Double.kelvinToCelsius(): Int{
    return (this - KELVIN_TO_CELSIUS).toInt()
}

@BindingAdapter("formattedDate")
fun TextView.formattedDate(dateEpoch: Long){
    text = dateEpoch.formatDateFromSeconds("EEEE, MMMM d ")
}

@BindingAdapter("formattedHour")
fun TextView.formattedHour(dateEpoch: Long){
    text = dateEpoch.formatDateFromSeconds("HH:mm")
}

fun Long.formatDateFromSeconds(pattern: String): String{
    val date = Date(TimeUnit.SECONDS.toMillis(this))
    return SimpleDateFormat(pattern, Locale.ENGLISH).format(date)
}
