package jocksch.geovane.utils

import androidx.fragment.app.Fragment

fun Fragment.replaceWithAnimation(fragment: Fragment, container: Int ){
    fragmentManager?.beginTransaction()
        ?.setCustomAnimations(
            R.anim.slide_in_left,
            R.anim.slide_out_left,
            R.anim.slide_in_right,
            R.anim.slide_out_right
        )
        ?.replace(container, fragment, "ForecastDetailFragment")
        ?.addToBackStack(null)
        ?.commit()
}
