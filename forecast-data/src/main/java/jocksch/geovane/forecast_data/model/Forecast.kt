package jocksch.geovane.forecast_data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Embedded
import java.io.Serializable

@Entity(tableName = "Forecast")
data class Forecast(
    @PrimaryKey var dt: Long,
    var clouds: Int,
    var deg: Int,
    var humidity: Int,
    var pop: Double,
    var pressure: Int,
    var rain: Double,
    var snow: Double,
    var speed: Double,
    var sunrise: Int,
    var sunset: Int,
    @Embedded(prefix = "temp")
    var temp: Temp,
    var weather: ArrayList<Weather>
) : Serializable {
    val firstWeather: Weather
        get() = weather.maxBy { it.id } ?: weather.first()
}
