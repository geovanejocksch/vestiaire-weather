package jocksch.geovane.forecast_data.model

import com.google.gson.annotations.SerializedName

data class ForecastResponse(
    val city: City,
    val cnt: Int,
    val cod: String,
    @SerializedName("list")
    val forecasts: List<Forecast>,
    val message: Double
)
