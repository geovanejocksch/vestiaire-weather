package jocksch.geovane.forecast_data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Weather(
    val description: String,
    val icon: String,
    @PrimaryKey
    val id: Int,
    val main: String
) {

    val iconUrl: String
        get() = "https://openweathermap.org/img/wn/$icon@2x.png"
}
