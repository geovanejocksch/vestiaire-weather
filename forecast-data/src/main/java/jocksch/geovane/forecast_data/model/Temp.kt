package jocksch.geovane.forecast_data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import jocksch.geovane.utils.kelvinToCelsius

@Entity
data class Temp(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val day: Double,
    val eve: Double,
    val max: Double,
    val min: Double,
    val morn: Double,
    val night: Double
) {

    val maxCelsius get() = max.kelvinToCelsius()

    val minCelsius get() = min.kelvinToCelsius()
}
