package jocksch.geovane.forecast_data.model

import androidx.room.Entity

@Entity
data class Coord(
    val lat: Double,
    val lon: Double
)
