package jocksch.geovane.forecast_data.model

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class ListWeatherConverter {

    @TypeConverter
    fun fromListWeatherToStoredString(weathers: ArrayList<Weather>): String {
        return Gson().toJson(weathers)
    }

    @TypeConverter
    fun fromStoredStringToListWeather(storedString: String): ArrayList<Weather> {
        return if (storedString.isNullOrEmpty()) {
            arrayListOf()
        } else {
            val listType: Type = object : TypeToken<List<Weather?>?>() {}.type
            Gson().fromJson<ArrayList<Weather>>(storedString, listType)
        }
    }
}
