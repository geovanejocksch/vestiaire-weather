package jocksch.geovane.forecast_data.datasource

import jocksch.geovane.forecast_data.model.Forecast
import jocksch.geovane.forecast_data.service.ForecastService

class ForecastRemoteDataSource(private val forecastService: ForecastService) {

    suspend fun getForecasts(): List<Forecast> {
        val result = forecastService.getForecast()
        return result.forecasts
    }
}
