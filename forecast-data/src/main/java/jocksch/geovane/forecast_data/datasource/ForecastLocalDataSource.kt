package jocksch.geovane.forecast_data.datasource

import jocksch.geovane.forecast_data.dao.ForecastDao
import jocksch.geovane.forecast_data.model.Forecast

class ForecastLocalDataSource(private val forecastDao: ForecastDao) {

    fun saveAll(forecasts: List<Forecast>){
        forecastDao.insertAll(forecasts)
    }

    fun getAll(): List<Forecast>{
        return forecastDao.getAll()
    }

    fun deleteAllBefore(date: Long){
        forecastDao.deleteAllBefore(date)
    }

    fun updateAll(forecasts: List<Forecast>){
        forecastDao.updateAll(forecasts)
    }
}
