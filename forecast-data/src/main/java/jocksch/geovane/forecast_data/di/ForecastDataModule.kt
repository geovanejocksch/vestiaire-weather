package jocksch.geovane.forecast_data.di

import androidx.room.Room
import jocksch.geovane.forecast_data.dao.AppDatabase
import jocksch.geovane.forecast_data.datasource.ForecastLocalDataSource
import jocksch.geovane.forecast_data.datasource.ForecastRemoteDataSource
import jocksch.geovane.forecast_data.repository.ForecastRepository
import jocksch.geovane.forecast_data.service.ForecastService
import jocksch.geovane.utils.api.getApi
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val forecastDataModule = module {
    factory { ForecastRepository(get(), get()) }
    factory { ForecastLocalDataSource(get()) }
    factory { ForecastRemoteDataSource(get()) }
    factory { getApi(ForecastService::class.java) }
    single {
        Room.databaseBuilder(
            androidContext(),
            AppDatabase::class.java,
            "vestiaire-wheather"
        ).build().forecastDao()
    }
}
