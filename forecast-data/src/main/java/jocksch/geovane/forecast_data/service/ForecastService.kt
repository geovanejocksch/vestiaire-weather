package jocksch.geovane.forecast_data.service

import jocksch.geovane.forecast_data.model.ForecastResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ForecastService {

    @GET("forecast/daily/")
    suspend fun getForecast(
        @Query("q") city: String = "Paris",
        @Query("cnt") cnt: Int = 16
    ): ForecastResponse
}
