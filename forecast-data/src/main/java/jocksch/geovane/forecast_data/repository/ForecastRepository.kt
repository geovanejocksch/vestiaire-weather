package jocksch.geovane.forecast_data.repository

import jocksch.geovane.forecast_data.datasource.ForecastRemoteDataSource
import jocksch.geovane.forecast_data.datasource.ForecastLocalDataSource
import jocksch.geovane.forecast_data.model.Forecast
import java.util.Date
import java.util.Calendar.YEAR
import java.util.Calendar.DAY_OF_MONTH
import java.util.Calendar.MONTH
import java.util.Calendar
import java.util.concurrent.TimeUnit

class ForecastRepository(
    private val forecastRemoteDataSource: ForecastRemoteDataSource,
    private val forecastLocalDataSource: ForecastLocalDataSource
) {

    /**
     * Here is where the magic happens.
     * The weather forecast changes every day once it is just a forecast, and not an exact science.
     * So, what can we do about it?
     * 1 - Get all forecasts we have locally, ordered by day.
     * If the first date is today, that means our data were loaded today and are probably updated,
     * so we just return it.
     * 2 - If the first date is before today, then we need to remove the old data
     * (there's no reason to keep old forecasts)
     * and update the local with the new values from api.
     */
    suspend fun getMostRecentForecasts(): List<Forecast> {
        val localData = forecastLocalDataSource.getAll()
        val today = System.currentTimeMillis()
        return when {
            localData.isEmpty() -> getFromApiAndInsertCache()
            localData.first().dt.isToday() -> localData
            else -> getFromApiAndUpdateCache(today)
        }
    }

    private suspend fun getFromApiAndInsertCache(): List<Forecast> {
        val response = forecastRemoteDataSource.getForecasts()
        forecastLocalDataSource.saveAll(response)
        return response
    }

    private suspend fun getFromApiAndUpdateCache(todayInMillis: Long): List<Forecast> {
        val response = forecastRemoteDataSource.getForecasts()
        forecastLocalDataSource.deleteAllBefore(TimeUnit.MILLISECONDS.toSeconds(todayInMillis))
        forecastLocalDataSource.updateAll(response)
        return response
    }
}

fun Long.isToday(): Boolean {
    val compareDay = Calendar.getInstance()
    compareDay.time = Date(this)

    val today = Calendar.getInstance()
    today.time = Date()
    return compareDay.get(DAY_OF_MONTH) == today.get(DAY_OF_MONTH)
            && compareDay.get(MONTH) == today.get(MONTH)
            && compareDay.get(YEAR) == today.get(YEAR)
}
