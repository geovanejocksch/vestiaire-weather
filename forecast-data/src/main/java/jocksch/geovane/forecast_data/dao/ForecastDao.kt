package jocksch.geovane.forecast_data.dao

import androidx.room.Insert
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Update
import jocksch.geovane.forecast_data.model.Forecast

@Dao
interface ForecastDao {

    @Insert
    fun insertAll(forecast: List<Forecast>)

    @Query("DELETE FROM forecast WHERE forecast.dt < :date")
    fun deleteAllBefore(date: Long)

    @Query("SELECT * FROM forecast ORDER BY forecast.dt")
    fun getAll(): List<Forecast>

    @Update
    fun updateAll(forecasts: List<Forecast>)
}
