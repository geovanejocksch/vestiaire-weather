package jocksch.geovane.forecast_data.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import jocksch.geovane.forecast_data.model.Forecast
import jocksch.geovane.forecast_data.model.Weather
import jocksch.geovane.forecast_data.model.Temp
import jocksch.geovane.forecast_data.model.ListWeatherConverter

@Database(entities = [Forecast::class, Weather::class, Temp::class], version = 1)
@TypeConverters(ListWeatherConverter::class)
abstract class AppDatabase: RoomDatabase(){
    abstract fun forecastDao(): ForecastDao
}
