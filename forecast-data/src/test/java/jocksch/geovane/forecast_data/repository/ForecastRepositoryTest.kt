package jocksch.geovane.forecast_data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import jocksch.geovane.forecast_data.datasource.ForecastRemoteDataSource
import jocksch.geovane.forecast_data.model.Forecast
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class ForecastRepositoryTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @MockK
    private lateinit var forecastRemoteDataSource: ForecastRemoteDataSource

    private lateinit var forecastRepository: ForecastRepository

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        forecastRepository = ForecastRepository(forecastRemoteDataSource)
    }

    @Test
    fun `should load forecast and return`() {

        val expectedResponse = mockk<List<Forecast>>()

        every {
            runBlocking {
                forecastRemoteDataSource.getForecasts()
            }
        } returns expectedResponse

        runBlocking {
            val response = forecastRepository.getMostRecentForecasts()
            assertEquals(response, expectedResponse)
        }
    }

}
