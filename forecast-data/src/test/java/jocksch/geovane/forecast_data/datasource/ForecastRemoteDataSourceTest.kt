package jocksch.geovane.forecast_data.datasource

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import jocksch.geovane.forecast_data.model.Forecast
import jocksch.geovane.forecast_data.model.ForecastResponse
import jocksch.geovane.forecast_data.service.ForecastService
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class ForecastRemoteDataSourceTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @MockK
    private lateinit var forecastService: ForecastService

    private lateinit var forecastRemoteDataSource: ForecastRemoteDataSource

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        forecastRemoteDataSource = ForecastRemoteDataSource(forecastService)
    }

    @Test
    fun `should load forecast and return`() {

        val expectedList = mockk<List<Forecast>>()

        val expectedResponse = ForecastResponse(mockk(), 1, "", expectedList, 0.0)

        every {
            runBlocking {
                forecastService.getForecast()
            }
        } returns expectedResponse

        runBlocking {
            val response = forecastRemoteDataSource.getForecasts()
            assertEquals(response, expectedList)
        }
    }

}
